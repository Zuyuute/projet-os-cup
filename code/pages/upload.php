<?php
require '../includes/pdo.php';

try {
    $db = new PDO("mysql:host=$SRV;port=$PORT;dbname=$DB;charset=utf8",
                $USR,
                $MDP
            );
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}catch (PDOException $e) {
     echo 'Connexion échouée : ' . $e->getMessage();
}



// Vérifier si le formulaire a été soumis
if($_SERVER["REQUEST_METHOD"] == "POST"){
    // Vérifie si le fichier a été uploadé sans erreur.
    if(isset($_FILES["photo"]) && $_FILES["photo"]["error"] == 0){
        $allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "gif" => "image/gif", "png" => "image/png");
        $filename = $_FILES["photo"]["name"];
        $filetype = $_FILES["photo"]["type"];
        $filesize = $_FILES["photo"]["size"];

        $name = htmlspecialchars($_POST['name']);
        //Enleve les espaces du nom afin d'avoir un nom correct dans l'url
        $nameurl = str_replace(' ','',$name);
        $date = htmlspecialchars($_POST['date']);

        // Vérifie l'extension du fichier
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        if(!array_key_exists($ext, $allowed)) die("Error : Veuillez sélectionner un format de fichier valide.");

        // Vérifie la taille du fichier - 5Mo maximum
        $maxsize = 5 * 1024 * 1024;
        if($filesize > $maxsize) die("Error: La taille du fichier est supérieure à la limite autorisée.");

        // Vérifie le type MIME du fichier
        if(in_array($filetype, $allowed)){
            // Vérifie si le fichier existe avant de le télécharger.
            if(file_exists("../upload/" . $date . $nameurl . "." . $ext)){
                echo $date . $nameurl . " existe déjà.";
            } else{
                //ON MET NOTRE FICHIER DANS LE DOSSIER CORRESPONDANT
                move_uploaded_file($_FILES["photo"]["tmp_name"], "../upload/" . $date . $nameurl . "." . $ext);
                echo "Votre fichier a été téléchargé avec succès.";

                $url = "../upload/" . $date . $nameurl . "." . $ext;

                //INSERTION DANS LA BDD
                $request = $db -> prepare("INSERT INTO travaux VALUES (:nom, :date, :description, :categorie, :url)");
    
                $request -> execute(array(':nom'=>$_POST['name'], 
                                          ':date'=>$_POST['date'],
                                          ':description'=>$_POST['desc'],
                                          ':categorie'=>$_POST['categorie'],
                                          ':url'=> $url
                                        ));

            } 
        } else{
            echo "Error: Il y a eu un problème de téléchargement de votre fichier. Veuillez réessayer."; 
        }
    } else{
        echo "Error: " . $_FILES["photo"]["error"];
    }
}
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Formulaire d'upload de fichiers</title>
</head>
<body>
    <form action="upload.php" method="post" enctype="multipart/form-data">
        <h2>Upload Fichier</h2>
        <div>
        <label for="fileUpload">Fichier:</label>
        <input type="file" name="photo" id="fileUpload" required>
        </div>
        <!-- AJOUT -->
        <div>
        <label for="namefile">Nom</label>
        <input type="text" name="name" id="namefile" required>
        </div>

        <div>
        <label for="date">Date</label>
        <input type="date" name="date" id="date" required>
        </div>

        <!-- LISTE DEROULANTE -->
        <div>
        <label for="categorie">Catégorie:</label>
        <select name="categorie" id="categorie" required>
            <option value="">--Veuillez choisir une catégorie--</option>
            <option value="illustration">Illustration</option>
            <option value="observation">Observation</option>
            <option value="imagination">Imagination</option>
            <option value="modelisation">Modélisation</option>
            <option value="animation">Animation</option>
            <option value="graphisme">Graphisme</option>
        </select>
        </div>

        <div>
        <label for="description">Description:</label>
        <textarea name="desc" id="description"></textarea>
        </div>
        <!----------->
        <input type="submit" name="submit" value="Upload">
        

        <p><strong>Note:</strong> Seuls les formats .jpg, .jpeg, .gif, .png sont autorisés jusqu'à une taille maximale de 5 Mo.</p>
    </form>

    <a href="..">Retour à l'accueil</a>
</body>
</html>