<?php
require '../includes/pdo.php';

//ON SE CONNECTE A LA BASE DE DONNEES
try {
    $db = new PDO("mysql:host=$SRV;port=$PORT;dbname=$DB;charset=utf8",
                $USR,
                $MDP
            );
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}catch (PDOException $e) {
     echo 'Connexion échouée : ' . $e->getMessage();
}


//TEST UNITAIRE POUR SAVOIR SI ON EST BIEN CONNECTE A LA BASE DE DONNEES
//On a initialisé une table simple avec id et nom afin de savoir si nous
//sommes bien connectés à notre BDD
$request = $db -> prepare('SELECT * FROM test1');
$request -> execute();
$res = $request -> fetchAll();


//VERIFIE LA COLONNE ID
$i=0;
foreach($res as $ligne){
    $i++;
    if($ligne['id'] == $i){
        echo "Cas ".$i." : Vous êtes bien connecté à la base de données ! <br/>";
    } else {
        echo "Il y a un problème.";
    }
}


//VERIFIE LA COLONNE NOM
if($res[0][1] == 'bonjour'){
    echo "<br/> La base de données nous dit bonjour !";
} else {
    echo "La base de données est impolie.";
}

?>