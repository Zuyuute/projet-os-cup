<?php
require '../includes/pdo.php';

try {
    $db = new PDO("mysql:host=$SRV;port=$PORT;dbname=$DB;charset=utf8",
                $USR,
                $MDP
            );
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}catch (PDOException $e) {
     echo 'Connexion échouée : ' . $e->getMessage();
}
?>


<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Formulaire d'upload de fichiers</title>
</head>
<body>
    
    <h1>Travaux</h1>

    <form action="affichage.php" method="post" enctype="multipart/form-data">
        <label for="categorie">Catégorie:</label>
        <select name="categorie" id="categorie" required>
            <option value="tout">Tout</option>
            <option value="illustration">Illustration</option>
            <option value="observation">Observation</option>
            <option value="imagination">Imagination</option>
            <option value="modelisation">Modélisation</option>
            <option value="animation">Animation</option>
            <option value="graphisme">Graphisme</option>
        </select>
        <input type="submit" name="submit" value="Trier">
    </form>

    <?php 
        if($_POST['categorie']=='tout'){
            $request = $db -> prepare('SELECT * FROM travaux ORDER BY travaux.date');
            $request -> execute();
            $tab = $request -> fetchAll();
        } else {
            $cat = $_POST['categorie'];
            $request = $db -> prepare('SELECT * FROM travaux WHERE categorie = :cat ORDER BY travaux.date');
            $request -> execute(array(':cat'=>$cat));
            $tab = $request -> fetchAll();
        }

        foreach($tab as $ligne){
            echo '
            <section>
                <h3>'.$ligne['nom'].'</h3>
                <img src="'.$ligne['url'].'" width="500px"/>
                <p><strong>'.$ligne['date'].'</strong>      '.$ligne['description'].'</p>
            </section>
            ';
        }
    ?>

    <a href="..">Revenir à l'accueil</a>

</body>
</html>